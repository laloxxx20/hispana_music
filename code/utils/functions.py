# -*- coding: utf-8 -*-
import smtplib
from email.mime.text import MIMEText as text
from django.utils.text import capfirst
from easy_thumbnails.files import get_thumbnailer
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def send_emails(sender, receivers, message, subject):
    sender = sender
    m = text(message)
    m['Subject'] = subject
    m['From'] = sender
    emails = ', '.join(receivers)
    m['To'] = emails

    try:
        smtpObj = smtplib.SMTP('smtp.gmail.com:587')
        smtpObj.starttls()
        smtpObj.login('ninjaxdev4@gmail.com', 'ninjaxdev44')
        smtpObj.sendmail(sender, receivers, str(m))
        print "Successfully sent email"
    except smtplib.SMTPException, e:
        print str(e)
        print "Error: unable to send email"


def creating_albums(object_list):
    answer = dict()
    for i, album in enumerate(object_list):
        answer[i] = {
                    'pk': album.id,
                    'artist_id': album.artist_id,
                    'name': capfirst(album.name),
                    'image': str(get_thumbnailer(album.image)),
                    }
    return answer


def paginator(list_to, paginate_by, page):
    paginator = Paginator(list_to, paginate_by)
    try:
        paginated = paginator.page(page)
    except PageNotAnInteger:
        paginated = paginator.page(1)
    except EmptyPage:
        paginated = paginator.page(paginator.num_pages)

    return paginated, paginator.page_range, paginator.num_pages
