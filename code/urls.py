from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
# from django.contrib.auth import views

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('social_auth.urls')),
    # url(r'^secret/', include('backend.urls', namespace="backend")),
    url(r'^', include('frontend.urls', namespace="frontend")),
    url(r'^', include('accounts.urls', namespace="accounts")),
    url(r'^online/', include('online_status.urls')),
    url('^accounts/', include('django.contrib.auth.urls')),
    # url(
    #     r'^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    #         'accounts.views.reset_confirm', name='password_reset_confirm'),
)

if settings.INCLUDE_STATIC:
    urlpatterns = patterns(
        '',
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT}),
        url(r'', include('django.contrib.staticfiles.urls')),
    ) + urlpatterns
