# -*- coding: utf-8 -*-
from django import forms
from .models import Song
import ftplib
from django.template.defaultfilters import slugify


class UploadForm(forms.ModelForm):

    upload_field = forms.FileField(required=False)

    def save(self, commit=True):
        slug_name = self.cleaned_data.get('slug')
        genre = self.cleaned_data.get('genre')
        artist = self.cleaned_data.get('artist')
        upload_field = self.cleaned_data.get('upload_field', None)
        pathname_first = 'public_html/mus24/' + slugify(genre.name)
        pathname_second = 'public_html/mus24/' + slugify(
            genre.name) + '/' + slugify(artist.name)
        session = ftplib.FTP(
            'yaskemusica.info', 'musyaskk', '19.IbLx{GEL[')
        try:
            session.cwd(pathname_second)
        except Exception:
            try:
                session.mkd(pathname_first)
                session.mkd(pathname_second)
                session.cwd(pathname_second)
            except Exception:
                session.mkd(pathname_second)
                session.cwd(pathname_second)
        if upload_field:
            name_to_save = 'STOR ' + slug_name + '.mp3'
            session.storbinary(name_to_save, upload_field)
            session.quit()
            self.instance.url = 'http://yaskemusica.info/mus24/' + slugify(
                genre.name) + '/' + slugify(
                    artist.name) + '/' + slug_name + '.mp3'
        else:
            self.instance.url = 'http://yaskemusica.info/mus24/' + slugify(
                genre.name) + '/' + slugify(
                    artist.name) + '/' + slug_name + '.mp3'
        return super(UploadForm, self).save(commit=commit)

    class Meta:
        model = Song
