# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django_extensions.db.fields import CreationDateTimeField
from easy_thumbnails.fields import ThumbnailerImageField


class Genre(models.Model):
    name = models.CharField(
        max_length=128,
        unique=True,
        verbose_name=(u'Nombre')
    )
    slug = models.SlugField(max_length=128)
    artists = models.ManyToManyField('backend.Artist', null=True, blank=True)

    class Meta:
        verbose_name = 'Género'
        verbose_name_plural = 'Géneros'

    def __unicode__(self):
        return self.name


class Artist(models.Model):
    CHOICES_RATE = (
        (1, '0'),
        (2, '1'),
        (3, '2'),
        (4, '3'),
        (5, '4'),
        (6, '5'),
    )
    name = models.CharField(max_length=128, verbose_name=(u'Nombre'))
    biography = models.TextField(
        null=True, blank=False, verbose_name=(u'Biografía'))
    primmarys_people = models.TextField(
        null=True, blank=True, verbose_name=(u'Cantantes Principales'))
    secundary_people = models.TextField(
        null=True, blank=True, verbose_name=(u'Integrantes'))
    rating = models.PositiveSmallIntegerField(
        choices=CHOICES_RATE,
        default=1,
        verbose_name=(u'Calificación')
    )
    image = ThumbnailerImageField(
        upload_to='photos',
        null=True,
        blank=True,
        verbose_name=(u'Imagen')
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )
    slug = models.SlugField()

    class Meta:
        verbose_name = 'Artista'
        verbose_name_plural = 'Artistas'

    def __unicode__(self):
        return self.name


class Album(models.Model):
    artist = models.ForeignKey(
        Artist,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=(u'Artista')
    )
    name = models.CharField(max_length=256, verbose_name=(u'Nombre'))
    year_of_publication = models.DateField(
        null=True,
        blank=True,
        verbose_name=(u'Año de Publicación')
    )
    producer = models.CharField(
        max_length=128,
        null=True,
        blank=True,
        verbose_name=(u'Productor')
    )
    image = ThumbnailerImageField(
        upload_to='photos',
        null=True,
        blank=True,
        verbose_name=(u'Imagen')
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )
    slug = models.SlugField()

    def __unicode__(self):
        return self.name


class Song(models.Model):
    artist = models.ForeignKey(
        Artist,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=(u'Artista')
    )
    genre = models.ForeignKey(
        Genre,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=(u'Género')
    )
    album = models.ForeignKey(
        Album,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=(u'Album')
    )
    original_title = models.CharField(
        max_length=256,
        verbose_name=(u'Título Original')
    )
    number_of_reproduction = models.IntegerField(
        default=0,
        verbose_name=(u'Número de Reproducción')
    )
    show_ringtone = models.BooleanField(
        default=True,
        verbose_name=(u'Mostrar Ringtone/Descarga')
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )
    url = models.CharField(max_length=256, null=True, blank=True)
    slug = models.SlugField(max_length=256)

    class Meta:
        verbose_name = (u'Canción')
        verbose_name_plural = (u'Canciones')

    def __unicode__(self):
        return self.original_title


class Lyric(models.Model):
    song = models.OneToOneField(
        Song,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=(u'Canción')
    )
    liryc = models.TextField(verbose_name=(u'Letra'))

    class Meta:
        verbose_name = 'Letra'
        verbose_name_plural = 'Letras'


class Video(models.Model):
    artist = models.ForeignKey(
        Artist,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=(u'Artista')
    )
    url = models.CharField(max_length=256)

    def __unicode__(self):
        return self.artist.name


class Playlist(models.Model):
    user = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=(u'Usuario')
    )
    created_at = CreationDateTimeField(verbose_name=(u'Creado en'))
    name = models.CharField(max_length=128, verbose_name=(u'Nombre'))

    class Meta:
        verbose_name = (u'Lista de Reproducción')
        verbose_name_plural = (u'Listas de Reproducción')

    def __unicode__(self):
        return self.name


class PlaylistDetail(models.Model):
    playlist = models.ForeignKey(
        Playlist,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name=(u'Lista de Reproducción')
    )
    song = models.ForeignKey(
        Song,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name=(u'Canción')
    )
