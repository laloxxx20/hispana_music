# -*- coding: utf-8 -*-
import os
import re
from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from optparse import make_option
from django.template.defaultfilters import slugify
from backend.models import Genre, Artist, Song

class Command(BaseCommand):
    help = "este es un comando para conseguir putas"
    option_list = BaseCommand.option_list + (
        make_option(
            "-p",
            "--path",
            dest="path",
            help="Lo que te voy a slugear",
            metavar="PATH"
        ),
    )


    def handle(self, *args, **options):
        if not options['path']:
            raise CommandError("Option `--path=...` must be specified.")
        path = options['path']
        routes_aux = []
        routes = []
        new_routes = []
        cont = 0
        archi=open(path,'r')

        while 1:
            linea=archi.readline()
            list_linea = linea.rsplit('\n', 1)
            linea = list_linea[0]
            routes.append(linea)
            if not linea:
                break
        archi.closed

        new_routes = [routes[i:i+3]for i in range(0, len(routes), 3)]

        if len(new_routes[-1:]) == 1:
            del new_routes[-1:]

        for new_route in new_routes:
            if not len(new_route) is 3:
                print "Ruta no válida"
            else:
                list_title = new_route[2].rsplit('.', 1)
                if new_route[1] != "mediaartlocal":
                    exist_genre = Genre.objects.filter(
                        slug=new_route[0]).first()
                    exist_artist = Artist.objects.filter(
                        slug=new_route[1]).first()
                    exist_song = Song.objects.filter(
                        slug=list_title[0]).first()
                    id_genre = None
                    id_artist = None
                    if not exist_genre:
                        genre = Genre.objects.create(
                            name=(re.sub('-+', ' ', new_route[0])),
                            slug=new_route[0])
                        id_genre = genre.pk
                    else:
                        id_genre = exist_genre.pk
                    if not exist_artist:
                        is_artist = Artist.objects.create(
                            name=(re.sub('-+', ' ', new_route[1])),
                            slug=new_route[1])
                        is_artist.Genre_set = Genre.objects.all()
                        id_artist = is_artist.pk
                    else:
                        id_artist = exist_artist.pk
                    if not exist_song:
                        title = re.sub('-+', ' ', list_title[0])
                        new_url = '%s/%s/%s/%s.%s' % (
                            'http://cumbiasur.info/mus', new_route[0], new_route[1], list_title[0], 'mp3')
                        Song.objects.create(
                            artist_id=id_artist,
                            genre_id=id_genre,
                            original_title=title,
                            slug=list_title[0],
                            url = new_url)
                    else:
                        exist_songname = Song.objects.filter(
                            slug=list_title[0])
                        aux = 0;
                        for exist in exist_songname:
                            if exist.artist_id == id_artist:
                                aux = 1
                        if aux == 1:
                            continue
                        else :
                            if exist_song.artist_id == id_artist:
                                continue
                            else:
                                title = re.sub('-+', ' ', list_title[0])
                                new_url = '%s/%s/%s/%s.%s' % (
                                    'http://cumbiasur.info/mus', new_route[0], new_route[1], list_title[0], 'mp3')
                                Song.objects.create(
                                    artist_id=id_artist,
                                    genre_id=id_genre,
                                    original_title=title,
                                    slug=list_title[0],
                                    url = new_url)
