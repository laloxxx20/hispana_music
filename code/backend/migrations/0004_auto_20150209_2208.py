# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0003_thumbnail_video'),
    ]

    operations = [
        migrations.CreateModel(
            name='SongImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('image', easy_thumbnails.fields.ThumbnailerImageField(null=True, upload_to=b'photos', blank=True)),
                ('song', models.ForeignKey(blank=True, to='backend.Song', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='thumbnail',
            name='song',
        ),
        migrations.DeleteModel(
            name='Thumbnail',
        ),
    ]
