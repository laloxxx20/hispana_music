# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0006_auto_20150213_0405'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='album',
            name='album',
        ),
        migrations.AddField(
            model_name='album',
            name='artist',
            field=models.ForeignKey(verbose_name='Artista', blank=True, to='backend.Artist', null=True),
            preserve_default=True,
        ),
    ]