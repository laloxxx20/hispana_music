# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lyric',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('liryc', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RenameField(
            model_name='playlist',
            old_name='created_date',
            new_name='created_at',
        ),
        migrations.RemoveField(
            model_name='album',
            name='year',
        ),
        migrations.RemoveField(
            model_name='artist',
            name='genre',
        ),
        migrations.AddField(
            model_name='album',
            name='producer',
            field=models.CharField(max_length=128, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='album',
            name='year_of_publication',
            field=models.DateField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='song',
            name='lyric',
            field=models.OneToOneField(null=True, blank=True, to='backend.Lyric'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='album',
            name='album',
            field=models.ForeignKey(blank=True, to='backend.Artist', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='genre',
            name='slug',
            field=models.SlugField(max_length=128),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='song',
            name='album',
            field=models.ForeignKey(blank=True, to='backend.Album', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='song',
            name='slug',
            field=models.SlugField(max_length=256),
            preserve_default=True,
        ),
    ]
