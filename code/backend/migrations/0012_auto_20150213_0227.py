# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0011_auto_20150213_0106'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='genre',
            options={'verbose_name': 'G\xe9nero', 'verbose_name_plural': 'G\xe9neros'},
        ),
        migrations.AddField(
            model_name='album',
            name='image',
            field=easy_thumbnails.fields.ThumbnailerImageField(upload_to=b'photos', null=True, verbose_name='Imagen', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='artist',
            name='created_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Creado en', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='artist',
            name='image',
            field=easy_thumbnails.fields.ThumbnailerImageField(upload_to=b'photos', null=True, verbose_name='Imagen', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='artist',
            name='rating',
            field=models.PositiveSmallIntegerField(default=1, verbose_name='Calificaci\xf3n', choices=[(1, b'0'), (2, b'1'), (3, b'2'), (4, b'3'), (5, b'4'), (6, b'5')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='song',
            name='number_of_reproduction',
            field=models.IntegerField(default=0, verbose_name='N\xfamero de Reproducci\xf3n'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='song',
            name='show_ringtone',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
