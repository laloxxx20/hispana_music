# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0005_auto_20150210_2037'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='songimage',
            name='song',
        ),
        migrations.DeleteModel(
            name='SongImage',
        ),
        migrations.AlterModelOptions(
            name='artist',
            options={'verbose_name': 'Artista', 'verbose_name_plural': 'Artistas'},
        ),
        migrations.AlterModelOptions(
            name='genre',
            options={'verbose_name': 'G\xe9nero', 'verbose_name_plural': 'G\xe9neros'},
        ),
        migrations.AlterModelOptions(
            name='lyric',
            options={'verbose_name': 'Letra', 'verbose_name_plural': 'Letras'},
        ),
        migrations.AlterModelOptions(
            name='playlistdetail',
            options={'verbose_name': 'Detalle de Palylist', 'verbose_name_plural': 'Detalles de Palylist'},
        ),
        migrations.AlterModelOptions(
            name='song',
            options={'verbose_name': 'Canci\xf3n', 'verbose_name_plural': 'Canciones'},
        ),
        migrations.AddField(
            model_name='artist',
            name='biography',
            field=models.TextField(null=True, verbose_name='Biograf\xeda'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='artist',
            name='primmarys_people',
            field=models.TextField(null=True, verbose_name='Cantantes Principales', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='artist',
            name='secundary_people',
            field=models.TextField(null=True, verbose_name='Integrantes', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='album',
            name='name',
            field=models.CharField(max_length=256, verbose_name='Nombre'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='album',
            name='producer',
            field=models.CharField(max_length=128, null=True, verbose_name='Productor', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='album',
            name='year_of_publication',
            field=models.DateField(null=True, verbose_name='A\xf1o de Publicaci\xf3n', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='artist',
            name='name',
            field=models.CharField(max_length=128, verbose_name='Nombre'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='genre',
            name='name',
            field=models.CharField(unique=True, max_length=128, verbose_name='Nombre'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lyric',
            name='liryc',
            field=models.TextField(verbose_name='Letra'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playlist',
            name='name',
            field=models.CharField(max_length=128, verbose_name='Nombre'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='song',
            name='original_title',
            field=models.CharField(max_length=256, verbose_name='T\xedtulo Original'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='video',
            name='song',
            field=models.ForeignKey(blank=True, to='backend.Artist', null=True),
            preserve_default=True,
        ),
    ]