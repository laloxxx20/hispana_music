# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0004_auto_20150209_2208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='song',
            name='artist',
            field=models.ForeignKey(to='backend.Artist', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='song',
            name='genre',
            field=models.ForeignKey(to='backend.Genre', null=True),
            preserve_default=True,
        ),
    ]
