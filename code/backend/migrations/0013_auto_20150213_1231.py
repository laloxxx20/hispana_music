# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0012_auto_20150213_0227'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='created_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Creado en', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='song',
            name='show_ringtone',
            field=models.BooleanField(default=True, verbose_name='Mostrar Ringtone/Descarga'),
            preserve_default=True,
        ),
    ]
