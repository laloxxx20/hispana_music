# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0014_merge'),
        ('backend', '0014_song_url'),
    ]

    operations = [
    ]
