# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0002_auto_20150206_0056'),
    ]

    operations = [
        migrations.CreateModel(
            name='Thumbnail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('path', easy_thumbnails.fields.ThumbnailerImageField(null=True, upload_to=b'photos', blank=True)),
                ('song', models.ForeignKey(blank=True, to='backend.Song', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(max_length=256)),
                ('song', models.ForeignKey(blank=True, to='backend.Song', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
