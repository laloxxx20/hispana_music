# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django.db.models.deletion
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0010_auto_20150213_0455'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='genre',
            options={'verbose_name': 'Genero', 'verbose_name_plural': 'Generos'},
        ),
        migrations.AlterModelOptions(
            name='playlist',
            options={'verbose_name': 'Lista de Reproducci\xf3n', 'verbose_name_plural': 'Listas de Reproducci\xf3n'},
        ),
        migrations.AlterModelOptions(
            name='playlistdetail',
            options={},
        ),
        migrations.RemoveField(
            model_name='song',
            name='lyric',
        ),
        migrations.AddField(
            model_name='lyric',
            name='song',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to='backend.Song', verbose_name='Canci\xf3n'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='album',
            name='artist',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='Artista', blank=True, to='backend.Artist', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playlist',
            name='created_at',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='Creado en', editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playlist',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='Usuario', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playlistdetail',
            name='playlist',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='Lista de Reproducci\xf3n', blank=True, to='backend.Playlist', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playlistdetail',
            name='song',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='Canci\xf3n', blank=True, to='backend.Song', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='song',
            name='album',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='Album', blank=True, to='backend.Album', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='song',
            name='artist',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='Artista', blank=True, to='backend.Artist', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='song',
            name='genre',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='G\xe9nero', blank=True, to='backend.Genre', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='video',
            name='artist',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='Artista', blank=True, to='backend.Artist', null=True),
            preserve_default=True,
        ),
    ]
