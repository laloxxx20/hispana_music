# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0009_auto_20150213_0453'),
    ]

    operations = [
        migrations.RenameField(
            model_name='video',
            old_name='song',
            new_name='artist',
        ),
    ]
