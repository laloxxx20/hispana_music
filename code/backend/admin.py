# -*- coding: utf-8 -*-
import HTMLParser
from django.contrib import admin
# from django.contrib.admin import RelatedOnlyFieldListFilter
from django.contrib.admin import SimpleListFilter
from backend.models import *
from .forms import UploadForm


class CountryFilter(SimpleListFilter):
    title = 'albúm'  # or use _('country') for translated title
    parameter_name = 'albúm'

    def lookups(self, request, model_admin):
        countries = set([c.country for c in model_admin.model.objects.all()])
        return [(c.id, c.name) for c in countries]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(country__id__exact=self.value())
        else:
            return queryset


class LyricInline(admin.StackedInline):
    model = Lyric


class InlineEditLinkMixin(object):
    edit_label = "Edit"

    def edit_details(self, obj):
        if obj.id:
            return obj.id
    edit_details.allow_tags = True


class SongAdmin(InlineEditLinkMixin, admin.ModelAdmin):
    # readonly_fields = ('upload_field',)
    form = UploadForm

    class Media:
        js = (
            'js/vendor/jquery.min.js',
            'js/vendor/jquery.slugify.js',
            'js/backend/admin_backend.js',
        )

    # list_filter = (CountryFilter,)

    search_fields = [
        'original_title',
        'genre__name',
        'artist__name',
        'album__name',
    ]
    list_per_page = 10
    list_display = (
        'id',
        'created_at',
        'original_title',
        'get_album',
        'get_artist',
        'get_genre',
    )

    # exclude = ('url',)

    inlines = [
        LyricInline,
    ]

    # Foreignkeys data
    def get_genre(self, obj):
        if not obj.genre:
            return obj.genre
        return obj.genre.name
    get_genre.short_description = 'Género'
    get_genre.admin_order_field = 'genre__name'

    def get_artist(self, obj):
        if not obj.artist:
            return obj.artist
        return obj.artist.name
    get_artist.short_description = 'Artista'
    get_artist.admin_order_field = 'artist__name'

    def get_album(self, obj):
        if not obj.album:
            return obj.album
        return obj.album.name
    get_album.short_description = 'Album'
    get_album.admin_order_field = 'album__name'

    # Save and Delete


class GenreAdmin(admin.ModelAdmin):

    class Media:
        js = (
            'js/vendor/jquery.min.js',
            'js/vendor/jquery.slugify.js',
            'js/backend/admin_backend.js',
        )

    search_fields = ['name']
    list_per_page = 10
    list_display = (
        'slug',
        'name',
    )

    # Foreignkeys data
    # Save and Delete


class ArtistAdmin(admin.ModelAdmin):
    class Media:
        js = (
            'js/vendor/jquery.min.js',
            'js/vendor/jquery.slugify.js',
            'js/backend/admin_backend.js',
        )

    search_fields = ['id', 'name', 'slug']
    list_per_page = 10
    list_display = ('id', 'name', 'created_at')


class VideoAdmin(admin.ModelAdmin):
    search_fields = ['id', 'artist__name', 'url']
    list_per_page = 10
    list_display = ('id', 'get_artist', 'url')

    def save_model(self, request, obj, form, change):
        urlVideo = form.cleaned_data['url']
        html_parser = HTMLParser.HTMLParser()
        html_video = html_parser.unescape(urlVideo)
        obj.url = html_video
        obj.save()

    def get_artist(self, obj):
        if obj.artist is None:
            return obj.artist
        return obj.artist.name
    get_artist.short_description = 'Artista'
    get_artist.admin_order_field = 'artist__name'


class AlbumAdmin(admin.ModelAdmin):
    class Media:
        js = (
            'js/vendor/jquery.min.js',
            'js/vendor/jquery.slugify.js',
            'js/backend/admin_backend.js',
        )

    search_fields = [
        'name',
        'year_of_publication',
        'producer',
        'artist__name',
    ]
    list_per_page = 10
    list_display = (
        'id',
        'name',
    )


# Register your models here.
admin.site.register(Genre, GenreAdmin)
admin.site.register(Artist, ArtistAdmin)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Song, SongAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Playlist)
