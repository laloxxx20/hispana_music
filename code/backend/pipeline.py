from social_auth.backends.facebook import FacebookBackend
from social_auth.backends.twitter import TwitterBackend
from social_auth.backends.google import GoogleOAuth2Backend
from django.contrib.auth.models import User
from accounts.models import UserProfile


def get_user_avatar(
    backend, details, response, social_user, uid, user, *args, **kwargs
):
    url = None
    if backend.__class__ == FacebookBackend:
        url = "http://graph.facebook.com/%s/picture?type=large" % response[
                'id']
    elif backend.__class__ == TwitterBackend:
        url = response.get('profile_image_url', '').replace('_normal', '')
    elif backend.__class__ == GoogleOAuth2Backend:
        if response.get('image') and response['image'].get('url'):
            url = response['image'].get('url')

    if url:
        user_current = User.objects.filter(username=user).first()
        profile, created = UserProfile.objects.get_or_create(
            user=user_current)
        profile.picture = url
        profile.save()
