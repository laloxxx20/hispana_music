# -*- coding: utf-8 -*-
from django import template
from backend.models import Genre
import random
import urllib

register = template.Library()


@register.inclusion_tag('utils/top_genres.html')
def top_genres():
    genres = Genre.objects.all()
    return dict(genres=genres)


@register.filter
def clean_genres(genres):
    try:
        return ', '.join([g.name for g in genres])
    except:
        return ', '.join([g for g in genres])


@register.filter
def shuffle(arg):
    tmp = list(arg)[:]
    random.shuffle(tmp)
    return tmp


@register.filter
def if_is_social(path):
    if "http" in path:
        return True
    return False


@register.filter
def drop_media(path):
    path = path.replace('/media/', '')
    return urllib.unquote(path)


@register.filter
def times(number):
    return range(number)


@register.filter
def placeholder(value, token):
    value.field.widget.attrs["placeholder"] = token
    return value


@register.filter
def class_html(value, token):
    value.field.widget.attrs["class"] = token
    return value
