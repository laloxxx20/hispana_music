from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

"""
Django settings for hispana project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'qgcjp*xxgp99!ka4b6x+^cy3xh-7=zyoyx0u(i9@hmf_xl+hhh'

# SECURITY WARNING: don't run with debug turned on in production!
# DEBUG = True
DEBUG = False

# TEMPLATE_DEBUG = True
TEMPLATE_DEBUG = False

# ALLOWED_HOSTS = []
ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'hizpana.com', 'www.hizpana.com']


TEMPLATE_DIRS = (
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, 'code/templates'),
)

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django.contrib.sites',

    # 3rd apps
    'django_extensions',
    'easy_thumbnails',
    'social_auth',
    'pure_pagination',

    # Local apps
    'accounts',
    'backend',
    'frontend',
    'online_status',
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'ninjaxdev4@gmail.com'
EMAIL_HOST_PASSWORD = 'ntiypzkjnrgvswlq'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'social_auth.middleware.SocialAuthExceptionMiddleware',
    'online_status.middleware.OnlineStatusMiddleware',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'default-cache'
    }
}

USERS_ONLINE__TIME_IDLE = 60*5  # 5 minutes
USERS_ONLINE__TIME_OFFLINE = 60*10  # 10 minutes
USERS_ONLINE__CACHE_PREFIX_USER = 'online_user'
USERS_ONLINE__CACHE_USERS = 'online_users'

# START Social Network Settigns
AUTHENTICATION_BACKENDS = (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'social_auth.backends.pipeline.associate.associate_by_email',
    'social_auth.backends.pipeline.user.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.user.update_user_details',
    'backend.pipeline.get_user_avatar',
)

LOGIN_URL = '/inicio_sesion/'
LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL = '/inicio_sesion/'

SOCIAL_AUTH_ENABLED_BACKENDS = ('twitter', 'facebook', 'google')
SOCIAL_AUTH_DEFAULT_USERNAME = 'new_social_auth_user'

TWITTER_CONSUMER_KEY = 'jcBplSUNOoM4JpjjYYl8AvM5Z'
TWITTER_CONSUMER_SECRET = 'OENaXHJQJhAJTfoccJ3vr8H7hLePtn9BAtVUUXcHG8DHZIQkdl'

FACEBOOK_APP_ID = '1108306079199033'
FACEBOOK_API_SECRET = 'e47902000a71e89c4ebca0b3273dae34'
FACEBOOK_EXTENDED_PERMISSIONS = ['email']

GOOGLE_OAUTH2_CLIENT_KEY = '1043023070966-2vjv2g2gkjlgnejcgqj9ejuu2vsrl66h.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET = 'uiOe2ZuKDL6m9ZbNsm6p2kPH'

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
# END Social Network Settigns

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'es-es'

TIME_ZONE = 'America/Lima'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'code/static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)


MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'django.contrib.auth.context_processors.auth',

    # 3rd apps
    "social_auth.context_processors.social_auth_by_type_backends",

)

LOGIN_URL = 'accounts:login'
INCLUDE_STATIC = True
CANT_MIN_CONTRASE = 6
TIME_REGISTRATION_KEY = 5
SITE_ID = 1

SUIT_CONFIG = {
    'ADMIN_NAME': 'Portal Hispana'
}

PAGINATION_SETTINGS = {
    'PAGE_RANGE_DISPLAYED': 6,
    'MARGIN_PAGES_DISPLAYED': 2,
}

# AUTH_USER_MODEL = 'accounts.User'
