import views
from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^$', views.index_main, name='index_main'),
    url(r'^index/$', views.index, name='index'),
    url(
        r'^find_fields_ajax/$', views.find_fields_ajax, name='find_fields_ajax'
    ),
    url(
        r'^see_artist/(?P<pk>[a-zA-Z0-9 \'&-]+)/$',
        views.see_artist,
        name='see_artist'),
    # url(
    #     r'^artist_albums/(?P<pk>[0-9]+)/$',
    #     views.AlbumByArtistListView.as_view(),
    #     name='artist_albums'
    # ),
    url(r'^genres/$', views.ArtistByGenresListView.as_view(), name='genres'),
    url(
        r'^genres/(?P<name_genre>[a-zA-Z0-9 \'&-]+)/$',
        views.ArtistByGenresListView.as_view(),
        name='get_genre'
    ),
    url(r'^artists/$', views.ArtistListView.as_view(), name='artists'),
    url(
        r'^artists/(?P<letter>\w{1})/$',
        views.ArtistListView.as_view(),
        name='get_artist'
    ),
    url(r'^perfil/$', views.perfil, name='perfil'),
    url(
        r'^galeria/(?P<pk>[0-9]+)/$',
        views.GaleriaListView.as_view(),
        name='galeria'),
    url(
        r'^videos/(?P<pk>[0-9]+)/$',
        views.videos,
        name='videos'
    ),
    url(
        r'^get_playlist_ajax/$',
        views.get_playlist_ajax,
        name='get_playlist_ajax'
    ),
    url(
        r'^set_playlist_ajax/$',
        views.set_playlist_ajax,
        name='set_playlist_ajax'
    ),
    url(
        r'^get_lyric_ajax/$',
        views.get_lyric_ajax,
        name='get_lyric_ajax'
    ),
    url(
        r'^get_songs_artist_ajax/$',
        views.get_songs_artist_ajax,
        name='get_songs_artist_ajax'
    ),
    url(
        r'^exist_videos_ajax/$',
        views.exist_videos_ajax,
        name='exist_videos_ajax'
    ),
    url(
        r'^exist_galery_ajax/$',
        views.exist_galery_ajax,
        name='exist_galery_ajax'
    ),
    url(
        r'^get_album_ajax/$',
        views.get_album_ajax,
        name='get_album_ajax'
    ),
    url(
        r'^get_albums_by_artists_ajax/$',
        views.get_albums_by_artists_ajax,
        name='get_albums_by_artists_ajax'
    ),
    url(
        r'^create_playlist_ajax/$',
        views.create_playlist_ajax,
        name='create_playlist_ajax'
    ),
    url(
        r'^get_list_ajax/$',
        views.get_list_ajax,
        name='get_list_ajax'
    ),
    url(
        r'^get_songs_counter_ajax/$',
        views.get_songs_counter_ajax,
        name='get_songs_counter_ajax'
    ),
    url(
        r'^delete_playlist_ajax/$',
        views.delete_playlist_ajax,
        name='delete_playlist_ajax'
    ),
    url(
        r'^delete_song_ajax/$',
        views.delete_song_ajax,
        name='delete_song_ajax'
    ),
    url(
        r'^get_albums_to_admin/$',
        views.get_albums_to_admin,
        name='get_albums_to_admin'
    ),
)
