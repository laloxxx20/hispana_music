# -*- coding: utf-8 -*-
import json
from django.shortcuts import get_object_or_404, render
from django.http import(
    HttpResponse
)
from django.utils.text import capfirst
from django.template.defaultfilters import truncatewords as truncate_words
from django.views.generic.list import ListView
from json_views.views import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from ast import literal_eval
from django.db.models import F
from easy_thumbnails.files import get_thumbnailer
from utils.functions import creating_albums, paginator
from pure_pagination.mixins import PaginationMixin
from django.core.paginator import EmptyPage
from django.core.cache import cache
from online_status.status import CACHE_USERS
from django.db import connection
from django.db.models import Sum
from backend.models import (
    Album,
    Artist,
    Genre,
    Video,
    Song,
    Playlist,
    PlaylistDetail,
    Lyric,
)


def index_main(request):
    context = dict()
    list_songs = Song.objects.all().select_related(
        'artist'
    ).order_by('-created_at')[:20]
    context['list_songs'] = list_songs
    return render(request, "base.html", context)


@csrf_exempt
def get_lyric_ajax(request):
    if request.method == "POST":
        song_id = request.POST.get('song_id')
        lyric = Lyric.objects.filter(song_id=song_id).first()
        if lyric:
            answer = {'answer': lyric.liryc}
        else:
            answer = {'answer': "No existe letra para esta canción"}
        return HttpResponse(
                json.dumps(answer),
                content_type='application/json; charset=utf8'
                )


def index(request):
    cursor = connection.cursor()
    cursor.execute("""select distinct artist_id from (select artist_id,
     genre_id from backend_song order by created_at desc) as hola limit 9""")
    row = cursor.fetchall()
    song_artists_pks = []
    [song_artists_pks.append(element[0]) for element in row]
    song_artists_pks_tup = tuple(song_artists_pks)
    song_artists_pks_str = str(song_artists_pks_tup)
    song_artists_pks_str = song_artists_pks_str.replace("L", "")
    song_artists_pks_str = song_artists_pks_str.replace("None,", "")
    query = """select * from backend_genre_artists where
       artist_id IN %s""" % song_artists_pks_str
    cursor.execute(query)
    genres_pt = cursor.fetchall()
    list_genres_pt = list(genres_pt)

    song_artists = Artist.objects.all().filter(
        pk__in=song_artists_pks)

    answer = {}
    for artist in song_artists:
        for one in list_genres_pt:
            if artist.pk == one[2]:
                genre_name = Genre.objects.filter(pk=one[1]).first()
                try:
                    answer[artist.pk]
                except:
                    num_repro = Song.objects.values('artist').filter(
                        artist=artist.pk).annotate(
                        number_of_reproduction=Sum(
                            'number_of_reproduction')).first()
                    answer[artist.pk] = {
                        'pk': artist.pk,
                        'name': artist.name, 'image': artist.image,
                        'genres': [genre_name.name],
                        'number_repro': num_repro['number_of_reproduction']
                        }
                else:
                    answer[artist.pk]['genres'].append(genre_name.name)

    answer_final = []
    [answer_final.append(elem) for pk, elem in answer.items()]

    songs = Song.objects.all().select_related('artist').order_by(
        '-number_of_reproduction')[0:10]

    num_user = User.objects.filter(is_active=True).count()
    online_users = cache.get(CACHE_USERS)
    context = dict({
        'song_artists': answer_final,
        'songs': songs,
        'num_user': num_user,
        'online_users': len(online_users)
    })
    return render(request, "index_pos.html", context)


def see_artist(request, pk):
    artist = Artist.objects.filter(pk=pk).first()
    genres = Genre.objects.filter(artists=artist)
    primmarys_peo, secundary_peo = None, None
    if artist.primmarys_people:
        primmarys_peo = artist.primmarys_people.split(",")
        if artist.secundary_people:
            secundary_peo = artist.secundary_people.split(",")
    albums_ultimos = Album.objects.filter(artist=artist).order_by(
        '-created_at'
    )
    albums_ultimos = albums_ultimos[:3]
    song_reproduced = Song.objects.select_related('artist').filter(
        artist=artist).order_by(
        '-number_of_reproduction',
        '-created_at',
    )
    song_reproduced = song_reproduced[:3]
    artist_genres = artist.genre_set.all().order_by('?').first()
    if artist_genres:
        artist_related = artist_genres.artists.all().order_by('?')
        artist_related = artist_related[:3]
        artist_and_albums = [(
            a,
            Album.objects.filter(artist=a).count()
        ) for a in artist_related]
    else:
        artist_related = []
        artist_and_albums = []
    context = {
        'artist': artist,
        'genres': genres,
        'primmarys_peo': primmarys_peo,
        'secundary_peo': secundary_peo,
        'albums_ultimos': albums_ultimos,
        'song_reproduced': song_reproduced,
        'artist_and_albums': artist_and_albums,
    }
    return render(request, "see_artist.html", context)


class ArtistByGenresListView(PaginationMixin, ListView):
    model = Artist
    template_name = 'generos.html'
    paginate_by = 6

    def get_queryset(self):
        if hasattr(self, 'genre'):
            artists = self.genre.artists.all()
        else:
            artists = Artist.objects.all()
        return artists

    def get_context_data(self, **kwargs):
        context = super(ArtistByGenresListView, self).get_context_data(
            **kwargs)
        if hasattr(self, 'genre'):
            context['genre'] = self.genre
        return context

    def dispatch(self, *args, **kw):
        genre_slug = kw.pop('name_genre', '')
        if genre_slug:
            self.genre = get_object_or_404(Genre, slug=genre_slug)
        return super(ArtistByGenresListView, self).dispatch(*args, **kw)


class ArtistListView(PaginationMixin, ListView):
    model = Artist
    template_name = 'artist.html'
    paginate_by = 6

    def get_queryset(self):
        artists = Artist.objects.filter(name__istartswith=self.letter)
        return artists

    def dispatch(self, *args, **kw):
        self.letter = kw.pop('letter', 'A')
        return super(ArtistListView, self).dispatch(*args, **kw)


class GaleriaListView(ListView):
    model = Album
    template_name = 'galeria.html'
    paginate_by = 20

    def get_queryset(self):
        album = Album.objects.filter(artist_id=self.pk)
        return album

    def get_context_data(self, **kw):
        context = super(GaleriaListView, self).get_context_data(
            **kw)
        artist = Artist.objects.filter(pk=self.pk).first()
        context['artist'] = artist
        return context

    def dispatch(self, *args, **kw):
        self.pk = kw.pop('pk', '')
        return super(GaleriaListView, self).dispatch(*args, **kw)


def perfil(request):
    playlists = Playlist.objects.filter(user=request.user)
    canciones = [(
        playlist,
        PlaylistDetail.objects.filter(playlist=playlist).count()
    ) for playlist in playlists]
    context = {
        'canciones': canciones
    }
    return render(request, "perfil.html", context)


def videos(request, pk):
    video_artists = Video.objects.select_related('artist').filter(artist=pk)
    genres = Genre.objects.filter(artists=pk)
    song_artists = Song.objects.filter(artist=pk)
    data_artist = video_artists[0]

    context = dict({
        'video_artists': video_artists,
        'data_artist': data_artist,
        'genres': genres,
        'song_artists': song_artists
    })
    return render(request, "videos.html", context)


@csrf_exempt
def exist_videos_ajax(request):
    if request.method == "POST":
        pk = request.POST.get('pk_artist')
        video_artist = Video.objects.select_related('artist').filter(artist=pk)
        answer = {}
        if video_artist:
            answer = {"data": "full"}
        else:
            answer = {"data": "empty"}
        return HttpResponse(
            json.dumps(answer),
            content_type='application/json; charset=utf8'
        )


def find_songs(request, searching):
    return render(request, 'find_songs.html')


@csrf_exempt
def find_fields_ajax(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        artists = Artist.objects.filter(name__icontains=name)[:3]
        genres = Genre.objects.filter(name__icontains=name)[:3]
        songs = Song.objects.filter(original_title__icontains=name)[:3]
        vector = []
        if songs:
            songs_vector = []
            for s in songs:
                artist = Artist.objects.filter(pk=s.artist_id).first()
                each_one = {'id': s.pk,
                            'text': capfirst(artist.name),
                            'type': 'song',
                            'artist': truncate_words(
                                capfirst(s.original_title), 7),
                            'mp3': s.url,
                            'poster': str(get_thumbnailer(artist.image)),
                            'free': s.show_ringtone,
                            }
                songs_vector.append(each_one)
            songs_total = {'text': "Canciones", 'children': songs_vector}
            vector.append(songs_total)
        if artists:
            artists_vector = []
            for art in artists:
                each_one = {'id': art.pk,
                            'text': capfirst(art.name),
                            'imagen': str(get_thumbnailer(art.image)),
                            'type': 'artist',
                            'artist': '',
                            'mp3': '',
                            'poster': '',
                            'free': '',
                            }
                artists_vector.append(each_one)
            artist_total = {'text': "Artistas", 'children': artists_vector}
            vector.append(artist_total)
        if genres:
            genres_vector = []
            for gen in genres:
                each_one = {'id': gen.pk,
                            'text': capfirst(gen.name),
                            'slug': gen.slug,
                            'type': 'genre',
                            'artist': '',
                            'mp3': '',
                            'poster': '',
                            'free': '',
                            }
                genres_vector.append(each_one)
            genres_total = {'text': "Generos", 'children': genres_vector}
            vector.append(genres_total)
    print "vector: ", vector
    return HttpResponse(
        json.dumps(vector),
        content_type='application/json; charset=utf8'
    )


@csrf_exempt
def get_playlist_ajax(request):
    if request.method == 'POST':
        playlists = Playlist.objects.filter(
            user=request.user)
        if playlists:
            answer = {}
            for i, rpta in enumerate(playlists):
                answer[str(i)] = rpta.name
        else:
            answer = {"data": "empty"}
        return HttpResponse(
            json.dumps(answer),
            content_type='application/json; charset=utf8'
        )


@csrf_exempt
def set_playlist_ajax(request):
    if request.method == 'POST':
        lists = request.POST.getlist('lists[]')
        song_name = request.POST.get('song')
        if lists:
            for element in lists:
                playlist, createdd = Playlist.objects.get_or_create(
                    name=element.strip(),
                    defaults={'user': request.user})
                song = Song.objects.filter(
                    original_title=song_name.strip()).first()
                PlaylistDetail(playlist=playlist, song=song).save()
            answer = {"data": "full"}
        else:

            answer = {"data": "empty"}
        return HttpResponse(
            json.dumps(answer),
            content_type='application/json; charset=utf8'
            )


@csrf_exempt
def get_songs_artist_ajax(request):
    if request.method == 'POST':
        pk_artist = request.POST.get('pk_artist')
        print "pk_artist: ", pk_artist
        song_artists = Song.objects.select_related('artist').filter(
            artist__id=pk_artist)
        answer = {}
        if song_artists:
            for i, rpta in enumerate(song_artists):
                answer[i] = {
                            'title': truncate_words(capfirst(
                                rpta.original_title), 7),
                            'artist': capfirst(rpta.artist.name),
                            'mp3': rpta.url,
                            'poster': str(get_thumbnailer(rpta.artist.image)),
                            'oga': 'http://data/' + str(rpta.pk),
                            'free': rpta.show_ringtone
                            }
        else:
            answer = {"data": "empty"}
        print "answer: ", answer
        return HttpResponse(
            json.dumps(answer),
            content_type='application/json; charset=utf8'
        )


@csrf_exempt
def exist_galery_ajax(request):
    if request.method == "POST":
        pk = request.POST.get('pk_artist')
        album_artist = Album.objects.filter(artist=pk)
        answer = {}
        if album_artist:
            answer = {"data": "full"}
        else:
            answer = {"data": "empty"}
        return HttpResponse(
            json.dumps(answer),
            content_type='application/json; charset=utf8'
        )


@csrf_exempt
def get_album_ajax(request):
    if request.method == 'POST':
        pk_album = request.POST.get('pk_album')
        pk_artist = request.POST.get('pk_artist')
        song_album_artist = Song.objects.select_related('artist').filter(
            artist_id=pk_artist, album_id=pk_album)
        answer = {}
        if song_album_artist:
            for i, song in enumerate(song_album_artist):
                answer[i] = {
                            'title': truncate_words(
                                capfirst(song.original_title), 7),
                            'artist': capfirst(song.artist.name),
                            'mp3': song.url,
                            'poster': str(get_thumbnailer(song.artist.image)),
                            'oga': 'http://data/' + str(song.pk),
                            'free': song.show_ringtone
                            }
        else:
            answer = {"data": "empty"}
        return HttpResponse(
            json.dumps(answer),
            content_type='application/json; charset=utf8'
        )


@csrf_exempt
def get_albums_by_artists_ajax(request):
    paginate_by = 6
    pk_artist = request.POST.get('pk_artist')
    page = request.POST.get('page', '')
    albums = Album.objects.filter(artist_id=pk_artist)
    albums, page_range, num_pages = paginator(albums, paginate_by, page)
    albums_1 = albums[len(albums)/2:]
    albums_2 = albums[:len(albums)/2]
    albums_1 = creating_albums(albums_1)
    albums_2 = creating_albums(albums_2)
    try:
        previous_page_num = albums.previous_page_number()
    except EmptyPage:
        previous_page_num = 0
    try:
        next_page_num = albums.next_page_number()
    except EmptyPage:
        next_page_num = 0

    return HttpResponse(json.dumps(
            {'albums1': albums_1,
             'albums2': albums_2,
             'has_previous': albums.has_previous(),
             'page_range': page_range,
             'number': albums.number,
             'previous_page_number': previous_page_num,
             'next_page_number': next_page_num,
             'num_pages': num_pages,
             'has_next': albums.has_next()}),
              content_type="application/json"
            )


@csrf_exempt
def create_playlist_ajax(request):
    pk_user = request.POST.get('pk_user')
    playlist = request.POST.get('playlist')
    Playlist.objects.create(name=playlist, user_id=pk_user)
    answer = {"created": "True"}
    return HttpResponse(json.dumps(answer), content_type="application/json")


@csrf_exempt
def get_list_ajax(request):
    if request.method == 'POST':
        pk_list = request.POST.get('pk_list')
        pk_user = request.POST.get('pk_user')
        song_list_user = PlaylistDetail.objects.select_related(
            'playlist', 'song', 'song__artist').filter(
            playlist__user_id=pk_user, playlist_id=pk_list)
        answer = {}
        if song_list_user:
            for i, song in enumerate(song_list_user):
                answer[i] = {
                            'title': truncate_words(
                                    capfirst(song.song.original_title), 7),
                            'artist': capfirst(song.song.artist.name),
                            'mp3': song.song.url,
                            'poster': str(get_thumbnailer(
                                song.song.artist.image)),
                            'oga': 'http://data/' + str(song.song.pk),
                            'free': song.song.show_ringtone
                            }
        else:
            answer = {"data": "empty"}
        return HttpResponse(
            json.dumps(answer),
            content_type='application/json; charset=utf8'
        )


@csrf_exempt
def get_songs_counter_ajax(request):
    if request.method == 'POST':
        dic_songs_counter = request.POST.get('dic_songs_counter')
        my_dict = literal_eval(dic_songs_counter)
        for pk, counter in my_dict.items():
            Song.objects.filter(pk=pk).update(
                number_of_reproduction=F('number_of_reproduction')+counter)
    answer = {"data": "empty"}
    return HttpResponse(
        json.dumps(answer),
        content_type='application/json; charset=utf8'
    )


@csrf_exempt
def delete_playlist_ajax(request):
    if request.method == 'POST':
        pk_album = request.POST.get('pk_album')
        print "pk_album: ", pk_album
        Playlist.objects.filter(pk=pk_album).delete()
        answer = {"data": "true"}
    else:
        answer = {"data": "false"}
    return HttpResponse(
        json.dumps(answer),
        content_type='application/json; charset=utf8'
    )


@csrf_exempt
def delete_song_ajax(request):
    if request.method == 'POST':
        pk_song = request.POST.get('pk_song')
        pk_playlist = request.POST.get('pk_playlist')
        print "pk_song: ", pk_song
        PlaylistDetail.objects.filter(
            song_id=pk_song, playlist_id=pk_playlist).delete()
        answer = {"data": "true"}
    else:
        answer = {"data": "false"}
    return HttpResponse(
        json.dumps(answer),
        content_type='application/json; charset=utf8'
    )


@csrf_exempt
def get_albums_to_admin(request):
    if request.method == 'POST':
        pk_artist = request.POST.get('artist')
        albums = Album.objects.filter(artist_id=pk_artist)
        answer = {}
        for i in albums:
            answer[str(i.pk)] = i.name
    return HttpResponse(
        json.dumps(answer),
        content_type='application/json; charset=utf8'
    )
