# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from .models import UserProfile
from django.conf import settings as sett
from utils.functions import send_emails
from django.contrib.sites.models import Site
import random
import string


class RegisterForm(forms.Form):
    email = forms.EmailField(
        max_length=75,
        required=True,
        widget=forms.TextInput(attrs={
            'id': 'email', 'name': 'email',
            'placeholder': 'Correo Electrónico',
            'class': 'form-control'
        }))
    password = forms.CharField(
        max_length=30, required=True,
        widget=forms.PasswordInput(attrs={
            'id': 'password', 'name': 'password',
            'placeholder': 'Contraseña',
            'class': 'form-control'
        }))
    password_two = forms.CharField(
        max_length=30, required=True,
        widget=forms.PasswordInput(attrs={
            'id': 'password', 'name': 'password',
            'placeholder': 'Confirmar Contraseña',
            'class': 'form-control '
        }))

    CHOICES_AVATAR = (
        (1, 'Personaje 1'),
        (2, 'Personaje 2'),
        (3, 'Personaje 3'),
        (4, 'Personaje 4'),
    )
    avatars = forms.ChoiceField(
        CHOICES_AVATAR,
        required=True,
        widget=forms.Select(
            attrs={
                'id': 'avatars', 'name': 'avatars',
                'class': 'form-control',
                'style': 'visibility:hidden',
            }))

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        password_two = self.cleaned_data.get('password_two')
        avatars = self.cleaned_data.get('avatars')
        usuario = User.objects.filter(username=email)
        if len(usuario) > 0:
            raise ValidationError(
                'El correo %s ya existe.' % email
            )
        if password != password_two:
            raise ValidationError(
                'Las claves ingresadas no son iguales.'
            )
        if password and len(password) < sett.CANT_MIN_CONTRASE:
            raise ValidationError(
                'Contraseña debe ser mayor a %s' % sett.CANT_MIN_CONTRASE
            )
        if not avatars:
            raise ValidationError(
                'Escoge un Personaje'
            )

        return self.cleaned_data

    def save(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        avatar = self.cleaned_data.get('avatars')
        key = ''.join(
            random.choice(
                string.ascii_lowercase + string.digits) for _ in range(30))
        current_site = Site.objects.get_current()
        message = """Para culminar tu registro por favor accede a el siguiente
        link y accede con los datos con los que te registraste:
        - http://%s/inicio_sesion/%s/
        Luego accede a tu cuenta:
        - Usuario: %s
        - Contrasena: %s""" % (current_site, key, email, password)

        send_emails(
            "ninjaxdev@gmail.com", email, message,
            "Confirmación de Registro Hispana")
        user = User(
            username=email,
            email=email,
            is_active=False,
        )
        user.set_password(password)
        user.save()
        profile = UserProfile()
        profile.user_id = user.pk
        profile.confirmation_key = key
        profile.avatar = avatar
        profile.save()


class EditProfileForm(forms.Form):
    password = forms.CharField(required=False)
    repassword = forms.CharField(required=False)
    avatar = forms.IntegerField(required=False)
    has_avatar = forms.CharField()
    picture = forms.ImageField(required=False)

    def clean_repassword(self):
        password = self.cleaned_data.get('password')
        repassword = self.cleaned_data.get('repassword')
        if password and not (password == repassword):
            raise ValidationError(
                u"Las contraseñas no coinciden"
            )
        if password and len(password) < 6:
            raise ValidationError(
                u"Ingresa una contraseña de más de 6 carácteres."
            )
        return password

    def has_newpassword(self):
        if self.cleaned_data.get('password'):
            return True
        return False

    def save(self, user):
        password = self.cleaned_data.get('password')
        avatar = self.cleaned_data.get('avatar')
        has_avatar = self.cleaned_data.get('has_avatar')
        picture = self.cleaned_data.get('picture')
        profile = user.userprofile
        if password:
            user.set_password(password)
            user.save()
        if has_avatar == "True" and avatar:
            profile.avatar = avatar
            profile.picture = ''
            profile.save()
        elif picture:
            profile.picture = picture
            profile.save()
        return dict(profile_image=profile.get_picture())
