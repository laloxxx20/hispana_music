from django.db import models
from django.conf import settings as sett
from django.contrib.auth.models import User
from datetime import timedelta, datetime
import time as tim
from easy_thumbnails.fields import ThumbnailerImageField
from django.templatetags.static import static


class UserProfile(models.Model):
    CHOICES_AVATAR = (
        (1, 'Personaje 1'),
        (2, 'Personaje 2'),
        (3, 'Personaje 3'),
        (4, 'Personaje 4'),
    )
    CHOICES_SOCIAL_NETWORK = (
        (1, 'Facebook'),
        (2, 'Twitter'),
        (3, 'Google+'),
        (4, 'Hispana'),
    )
    user = models.OneToOneField(User)
    confirmation_key = models.CharField(null=True, max_length=200)
    time_exp = datetime.now()+timedelta(days=sett.TIME_REGISTRATION_KEY)
    time_expiration = models.DecimalField(
        max_digits=15,
        decimal_places=2,
        default=tim.mktime(time_exp.timetuple()),
    )
    picture = ThumbnailerImageField(
        upload_to='photos',
        null=True,
        blank=True,
        verbose_name=(u'Imagen')
    )
    avatar = models.PositiveSmallIntegerField(
        null=True,
        blank=True,
        choices=CHOICES_AVATAR,
        default=1
    )
    social_network = models.PositiveSmallIntegerField(
        choices=CHOICES_SOCIAL_NETWORK,
        null=True,
        blank=True
    )

    def get_avatar_url(self):
        return static('img/avatar%i.jpg' % self.avatar)

    def get_picture(self):
        if self.picture:
            return self.picture.url
        return self.get_avatar_url()
