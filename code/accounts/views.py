# -*- coding: utf-8 -*-
import json
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import View
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.contrib.auth.views import password_reset, password_reset_confirm
from django.contrib.auth import (
    authenticate,
    login as auth_login,
    logout as auth_logout
)
from forms import (
    EditProfileForm,
    RegisterForm,
)
from models import UserProfile
from datetime import datetime


class RegisterView(View):
    form = RegisterForm
    template_name = "register.html"

    def get(self, request, *args, **kwargs):
        form = self.form()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('accounts:satisfy'))

        return render(request, self.template_name, {'form': form})

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(RegisterView, self).dispatch(*args, **kwargs)


def login(request, key="k"):
    if request.user.is_authenticated():
        return redirect(reverse('frontend:index_main'))
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        message = _(u'Nombre de usuario o contraseña inválido.')
        if username and password:
            user = authenticate(username=username, password=password)
            if user and user.is_active:
                auth_login(request, user)
                return redirect(reverse('frontend:index_main'))
            else:
                try:
                    profile = UserProfile.objects.get(user_id=user.pk)
                except:
                    messages.add_message(request, messages.ERROR, message)
                    return redirect('accounts:login')
                else:
                    tim = datetime.now()
                    if (profile.confirmation_key == key and
                            profile.time_exp >= tim):
                        auth_login(request, user)
                        user.is_active = True
                        user.save()
                        return redirect('frontend:index_main')
                    else:
                        return redirect("accounts:register")

    return render(request, 'login.html')


def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, request.FILES)
        is_valid = form.is_valid()
        context = dict()
        if is_valid:
            context['status'] = 'success'
            context['data'] = form.save(request.user)
            context['message'] = "Tus datos se han actualizado."
            if form.has_newpassword():
                return redirect(reverse('accounts:login'))
        else:
            context['status'] = 'error'
            context['message'] = "Por favor corrige los siguientes errores."
            context['data'] = form.errors

        return HttpResponse(json.dumps(context))
    else:
        form = EditProfileForm()
    return render(request, 'edit_profile.html', {'form': form})


def recover_pass(request):
    return password_reset(
            request, template_name='recover_pass.html',
            email_template_name='reset_email.html',
            subject_template_name='reset_subject.txt',
            post_reset_redirect=reverse('accounts:satisfy_recover')
            )


def reset_confirm(request, uidb64=None, token=None):
    return password_reset_confirm(
        request, template_name='reset_confirm.html',
        uidb64=uidb64, token=token,
        post_reset_redirect=reverse('accounts:satisfy_recover')
        )


def logout(request):
    auth_logout(request)
    return redirect('frontend:index_main')


def satisfy(request):
    return render(request, 'satisfy.html')


def satisfy_recover(request):
    return render(request, 'satisfy_recover.html')
