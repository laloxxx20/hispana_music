# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0007_auto_20150225_1711'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='avatar',
            field=models.PositiveSmallIntegerField(default=1, null=True, blank=True, choices=[(1, b'Personaje 1'), (2, b'Personaje 2'), (3, b'Personaje 3'), (4, b'Personaje 4')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='time_expiration',
            field=models.DecimalField(default=1425508908.0, max_digits=15, decimal_places=2),
            preserve_default=True,
        ),
    ]
