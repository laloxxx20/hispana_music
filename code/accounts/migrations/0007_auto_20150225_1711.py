# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_auto_20150218_1724'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='picture',
            field=easy_thumbnails.fields.ThumbnailerImageField(upload_to=b'photos', null=True, verbose_name='Imagen', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='time_expiration',
            field=models.DecimalField(default=1425334266.0, max_digits=15, decimal_places=2),
            preserve_default=True,
        ),
    ]
