# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='social_network',
            field=models.PositiveSmallIntegerField(blank=True, null=True, choices=[(1, b'Facebook'), (2, b'Twitter'), (3, b'Google+')]),
            preserve_default=True,
        ),
    ]
