# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_auto_20150303_1524'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='time_expiration',
            field=models.DecimalField(default=1425921369.0, max_digits=15, decimal_places=2),
            preserve_default=True,
        ),
    ]
