# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0011_auto_20150304_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='time_expiration',
            field=models.DecimalField(default=1426627592.0, max_digits=15, decimal_places=2),
            preserve_default=True,
        ),
    ]
