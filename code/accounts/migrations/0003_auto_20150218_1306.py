# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_userprofile_social_network'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='confirmation_key',
            field=models.CharField(max_length=200, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='avatar',
            field=models.PositiveSmallIntegerField(choices=[(1, b'Personaje 1'), (2, b'Personaje 2'), (3, b'Personaje 3'), (4, b'Personaje 4')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='social_network',
            field=models.PositiveSmallIntegerField(blank=True, null=True, choices=[(1, b'Facebook'), (2, b'Twitter'), (3, b'Google+'), (4, b'Hispana')]),
            preserve_default=True,
        ),
    ]
