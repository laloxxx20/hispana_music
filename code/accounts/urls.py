from django.conf.urls import patterns, url
# from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = patterns(
    '',
    url(
        r'^registrarse/$',
        views.RegisterView.as_view(), name='register'),
    url(
        r'^satisfactorio/$',
        views.satisfy, name='satisfy'),
    url(
        r'^satisfactorio_recobrar_email/$',
        views.satisfy_recover, name='satisfy_recover'),
    url(r'^inicio_sesion/$', views.login, name='login'),
    url(
        r'^inicio_sesion/(?P<key>[a-zA-Z0-9]\w+)/$',
        views.login, name='get_login'),
    url(r'^cerrar_sesion/$', views.logout, name='logout'),
    url(
        r'^editar_perfil/$',
        views.edit_profile, name='edit_profile'),
    url(
        r'^recobrar_contrasena/$',
        views.recover_pass, name='recover_pass'),    
)
