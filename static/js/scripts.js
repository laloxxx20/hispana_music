function menuHover() {
  $("#list-menu a").hover(
    function() {
      $( this ).animate( {backgroundColor:"#2C3E50"}, 20 );
    }, function() {
      $( this ).animate( {backgroundColor:"#16A085"}, 20 );
    }
  );

  $("#fui-list").click(function() {
    console.log( $("#sidebar-menu").css('left') )
    if($("#sidebar-menu").css('left') == "0px"){
      $("#sidebar-menu").animate({left:-210})
      $(".bx-prev").show()
    }else{
      $("#sidebar-menu").animate({left:0})
      $(".bx-prev").hide()
    }
  })

  // top List hover (index.html)
  $("#top-list li").hover(
    function() {
      $( this ).animate( {backgroundColor:"#1abc9c", color:"#FFFFFF"}, 100 );
      $( this ).children('a').animate( { color:"#FFFFFF"}, 50 );
    }, function() {
      $( this ).animate( {backgroundColor:"transparent", color:"#000000"}, 100 );
      $( this ).children('a').animate( { color:"#34495e "}, 50 );
    }
  );

  // hover en slider

  $(".bxslider li").hover(
    function() {
      $(this).children(".over-layer").show();
    }, function() {
      $(this).children(".over-layer").hide();
    }
  );

  $("#galeria .galeriaImg").hover(
    function() {
      $(this).children(".layer-galery").show();
    }, function() {
      $(this).children(".layer-galery").hide();
    }
  );

}

function resizeBodyContainer () {
  $("#body-container").width(  $("#container-hispana").width() -  $("#player-container").width()  - 2);
}

menuHover();
$('.bxslider').bxSlider( {
  mode: 'horizontal',
  captions: true,
  minSlides: 12,
  maxSlides: 12,
  auto: true,
  preloadImages: 'all',
  /*slideWidth: '1600px',*/
  nextText: '<i class="fa fa-angle-right"></i>',
  prevText: '<i class="fa fa-angle-left"></i>',
  autoHover: true,
});

  //scrollLyricsShow();
resizeBodyContainer();
$(window).resize(function(){
  resizeBodyContainer();
})

function ajax_message(message, status){
  var base_classes = "alert alert-dismissable ";
  var $alert = $('#message_super .alert');
  $alert.removeClass();
  if(status == "success"){
    $alert.addClass(base_classes + "alert-success");
  } else {
    $alert.addClass(base_classes + "alert-danger");
  }
  $('#message_super .message').html(message);
  $('#message_super').show('fast');
}


/**************************(S)TO ALL PAGES ********************************/
/**************************************************************************/
/*var icon ="<div style=\"margin-left:45%; margin-top:25%;\">"+
                  "<i class=\"fa fa-spinner fa-spin fa-5x\" ></i>"+
                "</div>";

  $(document).on('click', '.btn-href', function(){
    var href = $(this).data('href');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $('#body-container').html(icon);
    $('#body-container').load(href);
  });

  $(document).on('click', '.btn-page', function(){
    var page = $(this).data('page');    
    $('#body-container').load(page);
  });

  $(document).on('click', '.see_artist', function(){
    var href = $(this).data('see');
    $("html, body").animate({ scrollTop: 0 }, "slow", function(){
      $('#body-container').html(icon);
      $('#body-container').load(href);
    });
  });*/

/*$(document).on('click', '.videos', function(){
  var pk = $(this).data('pk');
  $.ajax({
    type: 'POST',
    url: URL_EXIST_VIDEO,
    data: {pk_artist: pk},
    async: false,
  })
  .done(function(res) {    
    exist = res.data;
    if(exist == "full"){
      var href = $('.videos').data('video');
      $('#body-container').load(href);
      $("html, body").animate({ scrollTop: 0 }, "slow");
    }
    else{
      mmessage = "No existen videos para este artista!";
      create_message(mmessage, DIV_MESSAGE);
    }
    $('#load-icon').hide();
  })
  .fail(function() {
    console.log( "error" );
  })
  .always(function() {
    console.log( "finished" );
  });
  
});


$('.play').popover({      
  trigger:'hover',
  content:"Cargarás todas las canciones de este artista en el reproductor",
  placement:'top',
});

$('.videos').popover({      
  trigger:'hover',
  content:"Verás todos los videos de este artista",
  placement:'top',
});

$('.galery').popover({      
  trigger:'hover',
  content:"Verás todos los albums del artista",
  placement:'top',
});

$('.see_artist').popover({      
  trigger:'hover',
  content:"Verás el perfil de este artista",
  placement:'top',
});*/

/**************************(S)TO ALL PAGES ********************************/
/**************************************************************************/
