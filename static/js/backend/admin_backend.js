$(function(){
    $('#id_slug').slugify('#id_name');    
    $('#id_slug').slugify('#id_original_title');
    $('#id_url').prop('disabled', true);
    $('#id_url').css("width","660px");
    
    var str = $('#id_artist').find('option:selected').attr('value');
    var url = "/get_albums_to_admin/";
    var options = $("#id_album");
    options.empty();
    $.post( url,{ artist: str }, function() {
      /*console.log( "success" );*/
    })
    .done(function(res) {
      $.each(res, function(key,value) {
          options.append("<option value=\""+key+"\">"+value+"</option>");
      });
      
    })
    .fail(function() {
      console.log( "error" );
    })
    .always(function() {
      console.log( "finished" );
    });

    $('#id_artist').change(function () {        
      var str = $(this).find('option:selected').attr('value');
      var url = "/get_albums_to_admin/";
      var options = $("#id_album");
      options.empty();
      $.post( url,{ artist: str }, function() {
        /*console.log( "success" );*/
      })
      .done(function(res) {
        $.each(res, function(key,value) {
            options.append("<option value=\""+key+"\">"+value+"</option>");
        });
        
      })
      .fail(function() {
        console.log( "error" );
      })
      .always(function() {
        console.log( "finished" );
      });
    });
});