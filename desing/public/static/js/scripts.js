$(document).ready(function(){
	menuHover();

	var jplayer = new jPlayerPlaylist({
		jPlayer: "#jquery_jplayer_1",
		cssSelectorAncestor: "#jp_container_1"
	},[
		{
			title:"Cro Magnon",
			artist:"Nacho Man",
			mp3:"http://www.jplayer.org/audio/mp3/TSP-01-Cro_magnon_man.mp3",
			oga:"http://www.jplayer.org/audio/ogg/TSP-01-Cro_magnon_man.ogg"
		},
		{
			title:"Beside Me",
			artist:"Nacho Man",
			mp3:"http://www.jplayer.org/audio/mp3/Miaow-06-Beside-me.mp3",
			oga:"http://www.jplayer.org/audio/ogg/Miaow-06-Beside-me.ogg"
		},
		{
			title:"Bubble",
			artist:"Nacho Man",
			mp3:"http://www.jplayer.org/audio/mp3/Miaow-07-Bubble.mp3",
			oga:"http://www.jplayer.org/audio/ogg/Miaow-07-Bubble.ogg"
		},
		{
			title:"Stirring of a Fool",
			artist:"Nacho Man",
			mp3:"http://www.jplayer.org/audio/mp3/Miaow-08-Stirring-of-a-fool.mp3",
			oga:"http://www.jplayer.org/audio/ogg/Miaow-08-Stirring-of-a-fool.ogg"
		},
		{
			title:"Partir",
			artist:"Nacho Man",
			mp3:"http://www.jplayer.org/audio/mp3/Miaow-09-Partir.mp3",
			oga:"http://www.jplayer.org/audio/ogg/Miaow-09-Partir.ogg"
		},
		{
			title:"Thin Ice",
			artist:"Nacho Man",
			mp3:"http://www.jplayer.org/audio/mp3/Miaow-10-Thin-ice.mp3",
			oga:"http://www.jplayer.org/audio/ogg/Miaow-10-Thin-ice.ogg"
		}
	], {
		swfPath: "../dist/jplayer",
		supplied: "oga, mp3",
		wmode: "window",
		useStateClassSkin: true,
		autoBlur: false,
		smoothPlayBar: true,
		keyEnabled: true,
		ready : function () {
			$(".jp-playlist ul li div").append('<div class="add">+</div>');
			$(".jp-previous").html("<span class='glyphicon glyphicon-backward' aria-hidden='true'></span>");
			$(".jp-play").html("<span class='glyphicon glyphicon-play' aria-hidden='true'></span>");
			$(".jp-stop").html("<span class='glyphicon glyphicon-stop' aria-hidden='true'></span>");
			$(".jp-next").html("<span class='glyphicon glyphicon-forward' aria-hidden='true'></span>");						
		},
		play:function() {
			$(".jp-play").html("<span class='glyphicon glyphicon-pause' aria-hidden='true'></span>");
		},
		pause:function() {
			$(".jp-play").html("<span class='glyphicon glyphicon-play' aria-hidden='true'></span>");
		}
	});
	


	$('.bxslider').bxSlider( {
        mode: 'horizontal',
        captions: true,
        minSlides: 12,
        maxSlides: 12,
        auto: true,
        preloadImages: 'all',
        slideWidth: '1600px',
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>',
        autoHover: true,
    });

	//scrollLyricsShow();
	resizeBodyContainer();
	$(window).resize(function(){
		resizeBodyContainer();
	})

	$('.bxsliderVideo').bxSlider({
  		pagerCustom: '#bx-pager'
	});

});

function menuHover() {
	$("#list-menu a").hover(
  		function() {
    		$( this ).animate( {backgroundColor:"#2C3E50"}, 20 );
  		}, function() {
    		$( this ).animate( {backgroundColor:"#16A085"}, 20 );
  		}
	);

	$("#fui-list").click(function() {
		console.log( $("#sidebar-menu").css('left') )
		if($("#sidebar-menu").css('left') == "0px"){
			$("#sidebar-menu").animate({left:-210})
			$(".bx-prev").show()
		}else{
			$("#sidebar-menu").animate({left:0})
			$(".bx-prev").hide()
		}
	})

	// top List hover (index.html)
	$("#top-list li").hover(
  		function() {
    		$( this ).animate( {backgroundColor:"#1abc9c", color:"#FFFFFF"}, 100 );
    		$( this ).children('a').animate( { color:"#FFFFFF"}, 50 );
  		}, function() {
    		$( this ).animate( {backgroundColor:"transparent", color:"#000000"}, 100 );
    		$( this ).children('a').animate( { color:"#34495e "}, 50 );
  		}
	);

	// hover en slider

	$(".bxslider li").hover(
  		function() {
  			$(this).children(".over-layer").show();
  		}, function() {
  			$(this).children(".over-layer").hide();
  		}
	);

	$("#galeria .galeriaImg").hover(
  		function() {
  			$(this).children(".layer-galery").show();
  		}, function() {
  			$(this).children(".layer-galery").hide();
  		}
	);	

}

function resizeBodyContainer () {
	$("#body-container").width(  $("#container-hispana").width() -  $("#player-container").width()  - 2);
}
/*
function scrollLyricsShow() {
	$( "#letter" ).hover(
  		function() {
    		$("#letter div").css({'overflow-y':'scroll'});
  		}, function() {
    		$("#letter div").css({'overflow-y':'hidden '});
  		}
	);	
}*/